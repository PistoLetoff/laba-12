class User < ApplicationRecord
  validates_uniqueness_of :nickname
  validates_length_of :nickname, minimum: 3
  validates_length_of :password, minimum: 3


  def has_password?(submitted_password)
    password == submitted_password
  end
  def self.authenticate(nickname, submitted_password)
    user = find_by_nickname(nickname)
    return nil  if user.nil?
    return user if user.has_password?(submitted_password)
  end
end
