class SessionsController < ApplicationController
  skip_before_action :authenticate, only: [:new, :create]

  def new
  end

  def create
    flash[:alert] = nil

    user = User.authenticate(params[:session][:nickname],
                             params[:session][:password])
    if user.nil?
      flash[:danger] = "Invalid nickname/password combination."
      @title = "Sign in"
      render 'new'
    else
      sign_in user
      redirect_to armstrong_input_path
    end
  end

  def destroy
    sign_out
    redirect_to root_path
  end
end
