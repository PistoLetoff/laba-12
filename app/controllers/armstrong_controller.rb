class ArmstrongController < ApplicationController
  def index
  end

  def input
  end

  def result
    @n = params[:number].to_i
    if @n < 1
      flash[:alert] = "There are't numbers with #{@n} digits"
      redirect_to armstrong_path
      return
    end

    if @n > 39
      @result = []
      return
    end

    if @n > 7
      flash[:alert] = "Very big number of digits. My computer will brake =("
      redirect_to armstrong_path
      return
    end

    @result = (10**(@n - 1)).upto(10**@n - 1).select do |num|
      num.digits.reduce(0) {|sum, dig| sum + dig ** @n}.equal? num
    end
  end
end
