Rails.application.routes.draw do
  get 'sessions/new'
  post 'sessions/create'
  get 'sessions/destroy'
  resources :users
  post 'users/create'
  post 'users/new'
  get 'users/show'
  get 'armstrong/input'
  get 'armstrong', to: redirect('armstrong/input')
  get 'armstrong/result', as: :result_armstrong

  root 'armstrong#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
